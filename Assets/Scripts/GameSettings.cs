using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class GameSettings : MonoBehaviour
{
    public static bool globalLight = false;
    public static bool sideLights = false;
    public static bool spotlight = false;
    public static bool cornerLights = false;
    public static bool shadowBall = false;

    public static bool getGlobalLight()
    {
        return globalLight;
    }

    public static bool getSideLights()
    {
        return sideLights;
    }

    public static bool getSpotlight()
    {
        return spotlight;
    }

    public static bool getCornerLights()
    {
        return cornerLights;
    }

    public static bool getShadowBall()
    {
        return shadowBall;
    }

    public static void reset()
    {
        globalLight = false;
        sideLights = false;
        spotlight = false;
        cornerLights = false;
        shadowBall = false;
    }
}
