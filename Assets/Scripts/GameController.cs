using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

/**
 * Gamecontroller controls the general flow of the game
 */
public class GameController : MonoBehaviour
{
    // GUI elements for score tallying
    public TextMeshProUGUI playerScoreText;
    public TextMeshProUGUI opponentScoreText;
    public GameSettings GameSettings;

    public void Start()
    {
        Debug.Log("Setting lights:");

        bool globalLight = GameSettings.getGlobalLight();
        Debug.Log("Global light:" + globalLight);
        if(!globalLight)
        {
            GameObject light = GameObject.FindGameObjectsWithTag("CenterLight")[0];
            light.SetActive(false);
        }

        bool sideLights = GameSettings.getSideLights();
        Debug.Log("Side lights:" + sideLights);
        if (!sideLights)
        {
            GameObject[] lightList = GameObject.FindGameObjectsWithTag("SideLights");
            foreach(GameObject light in lightList)
            {
                light.SetActive(false);
            }
        }

        bool spotlight = GameSettings.getSpotlight();
        Debug.Log("Spotlight:" + spotlight);
        if (!spotlight)
        {
            GameObject light = GameObject.FindGameObjectsWithTag("Spotlight")[0];
            light.SetActive(false);
        }

        bool cornerLights = GameSettings.getCornerLights();
        Debug.Log("Corner lights:" + cornerLights);
        if (!cornerLights)
        {
            GameObject[] lightList = GameObject.FindGameObjectsWithTag("CornerLights");
            foreach(GameObject light in lightList)
            {
                light.SetActive(false);
            }
        }

        bool shadowBall = GameSettings.getShadowBall();
        Debug.Log("Shadow ball:" + shadowBall);
        if (shadowBall)
        {
            GameObject ball = GameObject.FindGameObjectsWithTag("Ball")[0];
            ball.GetComponent<Renderer>().enabled = false;
        }
        Debug.Log("Lights set.");
        FindObjectOfType<BallMovement>().BallReset();
    }

    /**
     * StartRound prepares and starts the game
     */
    private void StartRound()
    {
        Debug.Log("Starting the next round");
        FindObjectOfType<BallMovement>().BallReset();
    }

    /**
     * EndRound updates the scores and calls StartRound -method
     */
    public void EndRound(string winner, int winnerScore)
    {
        Debug.Log("Round End, winner: " + winner);
        if(winner == "player")
        {
            playerScoreText.text = winnerScore.ToString();
        }
        else if (winner == "opponent")
        {
            opponentScoreText.text = winnerScore.ToString();
        }
        StartRound();
    }
}
