using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BallSpotlight : MonoBehaviour
{
    public GameObject spotlight;
    // Update is called once per frame
    void Update()
    {
        GameObject ball = GameObject.Find("Ball");
        transform.position = ball.transform.position;
    }
}
