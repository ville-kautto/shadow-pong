using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScene : MonoBehaviour
{
    public GameSettings GameSettings;

    public void PlayClassic()
    {
        Debug.Log("Loading classic pong");
        GameSettings.reset();
        GameSettings.globalLight = true;
        Play();
    }

    public void PlaySidelight()
    {
        Debug.Log("Loading side light pong");
        GameSettings.reset();
        GameSettings.sideLights = true;
        Play();
    }

    public void PlaySpotlight()
    {
        Debug.Log("Loading LightBall pong");
        GameSettings.reset();
        GameSettings.spotlight = true;
        Play();
    }

    public void PlayShadowPong()
    {
        Debug.Log("Loading Shadow pong");
        GameSettings.reset();
        GameSettings.shadowBall = true;
        Play();
    }

    public void Play()
    {
        Debug.Log("Loading Custom pong");
        SceneManager.LoadScene("GameScene");
    }

    public void MainMenu()
    {
        Debug.Log("Returning to Main Menu");
        SceneManager.LoadScene(0);
    }

}
