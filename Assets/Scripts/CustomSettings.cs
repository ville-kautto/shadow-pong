using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomSettings : MonoBehaviour
{
    public GameSettings GameSettings;

    public void ToggleGlobalLight()
    {
        GameSettings.globalLight = !GameSettings.globalLight;
        Debug.Log("Toggled global light:" + GameSettings.globalLight);
    }

    public void ToggleSideLights()
    {
        GameSettings.sideLights = !GameSettings.sideLights;
        Debug.Log("Toggled side lights:" + GameSettings.sideLights);
    }

    public void ToggleSpotlight()
    {
        GameSettings.spotlight = !GameSettings.spotlight;
        Debug.Log("Toggled spotlight:" + GameSettings.spotlight);
    }

    public void ToggleCornerLights()
    {
        GameSettings.cornerLights = !GameSettings.cornerLights;
        Debug.Log("Toggled corner lights:" + GameSettings.cornerLights);
    }

    public void ToggleShadowBall()
    {
        GameSettings.shadowBall = !GameSettings.shadowBall;
        Debug.Log("Toggled shadow ball:" + GameSettings.shadowBall);
    }
}
