using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoBall : MonoBehaviour
{
    // Ball 
    public Rigidbody2D ball;
    // MoveSpeed controls the movement speed of the ball
    public float moveSpeed = 5f;
    // StartPosition defines the starting position of the ball
    private Vector3 startPosition;
    // LastVelocity is used to calculate the ricochet direction of the ball
    private Vector3 lastVelocity;

    /**
     * Update continiously updates the ball's velocity
     */
    void Update()
    {
        lastVelocity = ball.velocity;
    }

    void Start()
    {
        BallReset();
        GameObject ball = GameObject.FindGameObjectsWithTag("Ball")[0];
        ball.GetComponent<Renderer>().enabled = false;
    }

    /**
     * BallReset controls the game flow
     * This method resets the ball position between rounds and manages the delay for the round start
     */
    public void BallReset()
    {
        startPosition = new Vector3(0, 0, 0);
        ball.velocity = new Vector3(0, 0, 0);
        ball.transform.position = startPosition;
        StartGame();
    }

    /**
     * Starts the game when called
     */
    void StartGame()
    {
        ball.AddForce(new Vector2(moveSpeed, 1));
    }


    /** 
     * OnCollisionEnter2D Handles the collisions with the ball
     * The direction of ricochet is depending on the properties of the ball 
     * The score changes if the ball hits the wall behind the players
     */
    void OnCollisionEnter2D(Collision2D collidedObject)
    {
        // Calculating the direction after a collision happens with the ball
        Debug.Log("Collision happened with" + collidedObject.collider.tag);
        var speed = lastVelocity.magnitude * (float)1.001;
        var direction = Vector3.Reflect(lastVelocity.normalized, collidedObject.contacts[0].normal);
        ball.velocity = direction * Mathf.Max(speed, 0f);
        Debug.Log(ball.velocity);
        // Upon hitting a goal, end the round and start a new one
        if (collidedObject.gameObject.CompareTag("Score"))
        {
            if (collidedObject.gameObject.name == "PlayerWall")
            {
                BallReset();
            }
            if (collidedObject.gameObject.name == "OpponentWall")
            {
                BallReset();
            }
        }
    }
}


